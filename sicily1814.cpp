//题目分析:
//不是什么难题，但是很注重你的逻辑和时序。
//我最讨厌这种题目了，这里填下那里补下，就像高中做的情况讨论，主要是闰年的问题。
//我的整体思路就是这样的，分成三部分计算，
//1.较小那个时间距离该年结束还有多少天，
//2.较大那时间从年头开始已经过了多少天，
//3.中间总共多少天。
//反正我是觉得自己是写得不咋的，我这个耗时0。29，而且代码冗余

//题目网址:http://soj.me/1814

#include <iostream>
#include <string>

using namespace std;

int dayOfMonth[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};


void getYearMonthDay (string str, int &y, int &m, int &d)
{
    y = int(str[0]-'0')*1000 + int(str[1]-'0')*100 + int(str[2]-'0')*10 + int(str[3]-'0');
    m = int(str[5]-'0')*10 + int(str[6]-'0');
    d = int(str[8]-'0')*10 + int(str[9]-'0');
    return;
}

bool runYear(int y)
{
    bool runYear = false;
    if (y%4 == 0 && y%100 != 0 || y%400 == 0) {
        runYear = true;
    }
    return  runYear;
}

void getTheDay(int y1, int y2, int m1, int m2, int d1, int d2)
{
    int daySum0 = 0;
    int daySum1 = 0;
    int daySum2 = 0;
    int tmpDaySum1 = 0;

    for (int i = y1+1; i <= y2-1; i++) {
        daySum0 += 365;
        if (runYear(i)) { daySum0++; }
    }

    for (int j = 1; j < m1; j++) {
        daySum1 += dayOfMonth[j];
    }
    daySum1 += d1;
    if (runYear(y1)) {
        if (m1 >= 3) 
            daySum1++;
    }
    tmpDaySum1 = daySum1;
    if (runYear(y1)) {
        daySum1 = 366 - daySum1;
    } else {
        daySum1 = 365 - daySum1;
    }

    for (int k = 1; k < m2; k++) {
        daySum2 += dayOfMonth[k];
    }
    daySum2 += d2;
    if (runYear(y2)) {
        if (m2 >= 3) 
            daySum2++;
    }

    if (y1 != y2) {
        cout << daySum0 + daySum1 + daySum2 << endl;
    } else {
        cout << daySum2 - tmpDaySum1 << endl;
    }

    return; 
}

int main()
{
    int testCase;
    string firstStr, secondStr;
    int year1, year2;
    int month1, month2;
    int day1, day2;

    cin >> testCase;
    while (testCase--) {
        cin >> firstStr >> secondStr;
        getYearMonthDay(firstStr, year1, month1, day1);
        getYearMonthDay(secondStr, year2, month2, day2);

        if ((year1 > year2) || (year1 == year2 && month1 > month2) || (year1 == year2 && month1 == month2 && day1 > day2)) {
            getTheDay(year2, year1, month2, month1, day2, day1);
        } else  {
            getTheDay(year1, year2, month1, month2, day1, day2);
        }
    }
    return 0;
}